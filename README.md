# [Peertube](https://github.com/Chocobozzz/PeerTube) on kubernetes

Federated (ActivityPub) video streaming platform using P2P (BitTorrent) directly in the web browser with WebTorrent.

---

## Status : WIP

- [x] Run Peertube and Redis
- [x] Use pvc to persist data
- [ ] Improve readme for deployment

## Before deploying

Currently if you want use persistent volume you need to create persistent volume and persistent volume claim. You may use sample claims from `sample`:  
 - change the storageclass from `ChangeMeStorageClass` to your correct class
 - apply the claim: `kubectl apply -f sample/claim.pvc-pt.yml`

You also need a postgresql server.  
If you know what you are doing and want to store postgres in Kubernetes, I suggest the excellent [stolon](https://github.com/helm/charts/tree/master/stable/stolon).

## Installing the chart

`$ helm install --name my-release .`

Specify each parameter using the --set key=value[,key=value] argument to helm install. For example,

```
$ helm install --name my-release \
  --set environment.hostname=new.domain.tld,\
  postgresql.postgresPassword=secretpassword \
  .
```

Alternatively, a YAML file that specifies the values for the parameters can be provided while installing the chart. For example,

`$ helm install --name my-release -f values.yaml .`

See the sample/values.yml for example of values to use

## Source

Fork from https://git.lecygnenoir.info/LecygneNoir/peertube-helm

Originated from https://github.com/MikaXII/helm-charts  
Thanks!
